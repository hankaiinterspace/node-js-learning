const fs = require('fs')

// fs.readFile('a.txt', (err, data) => {
//     console.log('第一个执行', data);
//     fs.readFile('b.txt', (err, data) => {
//         console.log('第二个执行', data);
//         fs.readFile('c.txt', (err, data) => {
//             console.log(data);
//         })
//     })
// })

// let promise = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         if (true) {
//             resolve({name: 'aaaaa'})
//         } else {
//             reject('失败了')
//         }
//     }, 2000);
// });

// promise.then(result => {
//     console.log(result);
// }).catch( err => {
//     console.log(err);
// })

// Promise对回调地狱的优化
function p1 () {
    return new Promise((resolve, reject) => {
        fs.readFile('a.txt', 'utf-8', (err, data) => {
            resolve(data)
        })
    })
}

function p2 () {
    return new Promise((resolve, reject) => {
        fs.readFile('b.txt', 'utf-8', (err, data) => {
            resolve(data)
        })
    })
}

function p3 () {
    return new Promise((resolve, reject) => {
        fs.readFile('c.txt', 'utf-8', (err, data) => {
            resolve(data)
        })
    })
}

p1().then(r1 => {
    console.log(r1);
    return p2()
}).then(r2 => {
    console.log(r2);
    return p3()
}).then(r3 => {
    console.log(r3);
})
