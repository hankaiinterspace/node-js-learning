function test (callback) {
    let a = 'test';
    callback(a);
}

function func (x) {
    let obj = {
        name: 'func'
    }
    console.log(obj.name + x);
}

test(func);