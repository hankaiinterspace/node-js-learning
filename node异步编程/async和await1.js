const fs = require('fs')
const util = require('util')
// promisify 处理完返回的是一个 promise 对象
const mineReadFile = util.promisify(fs.readFile);


async function main() {
    // 读取第一个文件的内容
    let data1 = await mineReadFile('./a.txt');
    let data2 = await mineReadFile('./b.txt');
    let data3 = await mineReadFile('./c.txt');
}