async function fn1() {
    return 'fn1'
}

async function fn2() {
    return 'fn2'
}

async function fn3() {
    return 'fn3'
}
// console.log(fn());
// fn().then(res => {
//     console.log(res);
// }).catch(err => {
//     console.log(err);
// })

async function run () {
    let r1 = await fn1();
    let r2 = await fn2();
    let r3 = await fn3();
    console.log(r1);
    console.log(r2);
    console.log(r3);
}

run()