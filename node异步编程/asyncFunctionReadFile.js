const fs = require('fs')
// fs.readFile() 这个方法不返回Promise对象
// promisify 可以对异步Api进行包装，让包装的方法返回Promise对象以支持 async await 语法
const promisify = require('util').promisify;
const readFile = promisify(fs.readFile);

async function run () {
    let r1 = await readFile('a.txt', 'utf-8')
    let r2 = await readFile('b.txt', 'utf-8')
    let r3 = await readFile('c.txt', 'utf-8')
    console.log(r1);
    console.log(r2);
    console.log(r3);
}
