const gulp = require('gulp')
// 使用gulp.task()方法建立任务
const htmlmin = require('gulp-htmlmin');
const fileinclude = require('gulp-file-include');
const less = require('gulp-less')
const csso = require('gulp-csso')
const babel = require('gulp-babel')
const uglify = require('gulp-uglify')

gulp.task('first', () => {
    console.log('第一个gulp-demo');
    // 获取要处理的文件
    gulp.src('./src/css/base.css')
    // 将处理后的文件输出到dist目录
    .pipe(gulp.dest('./dist/css'))
});
 
// html
gulp.task('minify', () => {
  return gulp.src('src/*.html')
    // 在压缩前先引入公共代码 gulp-file-include
    .pipe(fileinclude())
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest('dist'));
});

// css
gulp.task('less', () => {
  return gulp.src(['src/css/*.less', 'src/css/*.css'])
    // 将less语法转换为css语法
    .pipe(less())
    // 将css代码进行压缩
    .pipe(csso())
    // 将处理的结果进行输出
    .pipe(gulp.dest('dist/css'))
});

// js
gulp.task('jsmin', () =>
    gulp.src('src/js/*.js')
        .pipe(babel({
          // 它可以判断当前代码的运行环境，将代码转换成当前环境所需要的代码
            presets: ['@babel/env']
        }))
        // 压缩js代码
        .pipe(uglify())
        .pipe(gulp.dest('dist/js'))
);

// 复制文件夹
gulp.task('copy', () => {
    gulp.src('./src/images/*')
        .pipe(gulp.dest('dist/images'));
    gulp.src('./src/lib/*')
        .pipe(gulp.dest('dest/lib'));
});

// 构建任务
// gulp4指定写法
gulp.task('build', gulp.parallel('minify', 'less', 'jsmin', 'copy', () => {
  // build the website.
}));