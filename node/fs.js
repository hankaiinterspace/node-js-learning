const fs = require('fs')
fs.readFile('test.txt', 'utf8', function (err, dataStr) {
    console.log(err);
    console.log('---------');
    console.log(dataStr);
}) 


fs.readFile('./test.txt', 'utf8', function (err, result) {
    if (err) {
        console.log('文件读取失败' + err.message);
    }
    console.log('文件读取成功，内容是：' + result);
})

fs.writeFile(__dirname +'/test.txt', 'hello Node.js', function (err) {
    if (err) {
        console.log('文件写入失败');
    }
    console.log('文件写入成功');
})


// __dirname 表示当前文件所出的目录
fs.readFile(__dirname + '/test.txt', 'utf8', function (err, dataStr) {
    if (err) {
        console.log('读取文件失败');
    }
    console.log(dataStr);
})