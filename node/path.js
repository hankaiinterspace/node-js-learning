const path = require('path')

const pathStr = path.join('/a', '/b/c', '../', './d', 'e')
console.log(pathStr);


const pathStr2 = path.join(__dirname, './1.txt')
console.log(pathStr2);


const fpath = 'a/b/c/index.html' // 文件的存放路径

var fullName = path.basename(fpath)
console.log(fullName); // 输出 index.html

var nameWithoutExt = path.basename(fpath, '.html')
console.log(nameWithoutExt); // 输出 index


const fext = path.extname(fpath)
console.log(fext); // 输出 .html