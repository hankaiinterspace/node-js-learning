const http = require('http')
// 调用 http.createServer() 方法，即可快速创建一个 web 服务器实例：
const server = http.createServer()
// 为服务器实例绑定 request 事件，即可监听客户端发送过来的网络请求：
server.on('request', (req, res) => {
    // 只要又客户端来请求我们自己的服务器，就会触发request事件，从而调用这个事件处理函数
    const str = `your request url is ${req.url}, and request method is ${req.method} 中文不乱码`
    console.log('someone visit our web server');
    console.log(str);
    // 为了防止中文显示乱码的问题，需要设置响应头content-type的值为 text/html;charset=utf-8
    res.setHeader('Content-Type', 'text/html; charset=utf-8')
    // res.end()方法的作用
    // 向客户端发送指定内容，并结束这次请求的处理过程
    res.end(str);
})
// 调用服务器实例的 .listen() 方法，即可启动当前的 web 服务器实例：
server.listen(8080, () => {
    console.log('http server running at http://127.0.0.1');
})

