const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/playground')
    .then(() => console.log('数据库连接成功'))
    .catch(err => console.log('数据库连接失败', err))

// 创建集合规则
const userSchema = new mongoose.Schema({
    name: String,
    age: Number,
    email: String,
    password: String,
    hobbies: [String]
});

// 使用规则创建集合
const User = mongoose.model('User', userSchema);

// 查询用户集合中所有文档
// User.find().then(result => console.log(result))

// 根据id查询文档
// User.findOne({_id: '5c09f2d9aeb04b22f846096b'}).then(result => console.log(result))

// 查询用户集合中年龄字段大于20并且小于40的文档
// User.find({age: {$gt: 20, $lt: 40}}).then(result => console.log(result))


// 查询用户集合中匹配包含敲代码的文档
// User.find({hobbies: {$in: ['敲代码']}}).then(result => console.log(result))


// 根据年龄字段升序排列
// User.find().sort('age').then(result => console.log(result))

// skip 跳过多少条数据  limit 限制查询数量
User.find().skip(2).limit(2).then(result => console.log(result))