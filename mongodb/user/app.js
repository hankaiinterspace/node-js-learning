const express = require('express')
const app = express()
const staticRouter = require('./staticRouter')
// 导入cors中间件
const cors = require('cors')

// 先注册url-encoded中间件
app.use(express.urlencoded({ extended: false }))
app.use(express.json())
// 注册cors中间件
app.use(cors())

app.use('/static', staticRouter)



app.listen(8080, () => {
    console.log('Express server running at http://127.0.0.1');
})