const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/playground')
    .then(() => console.log('数据库连接成功'))
    .catch(err => console.log('数据库连接失败', err))


const postSchema = new mongoose.Schema({
    title: {
        type: String,
        // 必选字段
        required: true,
        // 字符串的最小长度
        minlength: 2,
        // 字符串的最大长度
        maxlength: 5,
        // 去除字符串两边的空格
        trim: true
    },
    age: {
        type: Number,
        // 数值最小
        min: 18,
        // 数值最大
        max: 100
    },
    publishDate: {
        type: Date,
        // 默认值
        default: Date.now
    },
    category: {
        type: String,
        // 枚举限定内容
        enum: ['html', 'css', 'javascript', 'node.js']
    },
    author: {
        type: String,
        validate: {
            validator: v => {
                // 返回布尔值
                // true 验证成功
                // false 验证失败
                // v 要验证的值
                return v && v.length > 4
            },
            // 自定义错误信息
            message: '传入的值不符合验证规则'
        }
    }
});

const Post = mongoose.model('Post', postSchema);
Post.create({title: 'abc', age: 60, category: 'javascript',author: 'aaaaa'}).then(res => console.log(res))