const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/playground')
    .then(() => console.log('数据库连接成功'))
    .catch(err => console.log('数据库连接失败', err))

// 设定集合规则
const courseSchema = new mongoose.Schema({
    name: String,
    author: String,
    isPublished: Boolean
});

// 创建集合并应用规则 Course 是返回来的一个构造函数
const Course = mongoose.model('Course', courseSchema); // courses

// 创建集合实例
// 第一种方式
// const course = new Course({
//     name: 'Node.js mongonDB',
//     author: 'gaohan',
//     isPublished: true
// });

// 将数据保存在数据库中
// course.save();


// 第二种方式, 可以用上Promise
Course.create({name: 'javascript', author: 'aaaaa', isPublished: true})
    .then(doc => console.log(doc))
    .catch(err => console.log(err))

