const path = require('path')

let str = path.resolve(__dirname, 'build')
// console.log(str);

module.exports = {
    entry: './src/index.js',
    output: {
        filename: 'built.js',
        path: str
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            },
            {
                test: /\.less$/,
                use: [
                    'style-loader',
                    'css-loader',
                    'less-loader'
                ]
            }
        ]
    },
    mode: 'development'
}