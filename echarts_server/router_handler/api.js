const { Budget, Seller, Trend, Map, Rank, Hot, Stock  } = require('../db/index')
const status = require('../store/index')
const path = require('path')
const fileUtils = require('../utils/file_utils')


exports.budget = (req, res) => {
    Budget.find().then(result => {
        res.send({
            status: 0,
            message: '获取budget数据成功',
            data: result
        })
    })
}

exports.seller = (req, res) => {
    Seller.find().then(result => {
        res.send({
            status: 0,
            message: '获取seller数据成功',
            data: result
        })
    })
}

exports.trend = (req, res) => {
    Trend.find().then(result => {
        res.send({
            status: 0,
            message: '获取trend数据成功',
            data: result
        })
    })
}

exports.updateRank = (req, res) =>{
    Rank.updateOne(req.body).then(result => {
        status.flag = true;
        status.chartName = 'rank'
        status.socketType = 'rankData'
        // console.log(status);
        res.send({
            status: 0,
            message: '修改rank数据成功'
        })
    })
}

exports.map = (req, res) => {
    Map.find().then(result => {
        res.send({
            status: 0,
            message: '获取trend数据成功',
            data: result
        })
    })
}


exports.rank = (req, res) => {
    Rank.find().then(result => {
        res.send({
            status: 0,
            message: '获取trend数据成功',
            data: result
        })
    })
}

exports.hot = (req, res) => {
    Hot.find().then(result => {
        res.send({
            status: 0,
            message: '获取trend数据成功',
            data: result
        })
    })
}

exports.stock = (req, res) => {
    Stock.find().then(result => {
        res.send({
            status: 0,
            message: '获取trend数据成功',
            data: result
        })
    })
}

exports.chinamap = async (req, res) => {
    let filePath = '../map/china.json'
    filePath = path.join(__dirname, filePath)
    const ret = await fileUtils.getFileJsonData(filePath)
    res.send({
        status: 0,
        message: '获取trend数据成功',
        data: ret
    })
}

exports.provincemap = async (req, res) => {
    let province = req.params.name;
    console.log('province', province);
    if (province == 'undefined') {
        res.send({
            status: 1,
            message: '请求数据失败'
        })
    } else {
        let filePath = `../map/province/${req.params.name}.json`
        filePath = path.join(__dirname, filePath)
        const ret = await fileUtils.getFileJsonData(filePath, province)
        // console.log(ret);
        res.send({
            status: 0,
            message: '获取trend数据成功',
            data: ret
        })
    }
}