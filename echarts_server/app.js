const express = require('express')
// 引入websocket
const webSocketService = require('./service/web_socket_service')
const app = express()

// 导入 cors 中间件
const cors = require('cors')
app.use(cors())

app.use(express.urlencoded({ extended: false }))

// 导入并注册路由模块
const apiRouter = require('./router/api')
app.use('/api', apiRouter)


app.listen(3007, () => {
    console.log('api server is running');
})

webSocketService.listen()

