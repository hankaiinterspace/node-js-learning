const mongoose = require('mongoose')

mongoose.connect('mongodb://8.142.6.23/echarts_server')
    .then(() => console.log('数据库连接成功'))
    .catch(err => console.log('数据库连接失败', err))


// 创建集合规则
const budgetSchema = new mongoose.Schema({
    dimName: String,
    dimZhName: String,
    max: Number,
    budget: Number,
    expense: Number
})

const Budget = mongoose.model('Budget', budgetSchema);


const sellerSchema = new mongoose.Schema({
    name: String,
    value: Number
})

const Seller = mongoose.model('Seller', sellerSchema);


const trendSchema = new mongoose.Schema({
    title: String,
    base: Number,
    unit: String,
    data: [
        {
            name: String,
            data: [String]
        }
    ]
})

const Trend = mongoose.model('Trend', trendSchema)


const mapSchema = new mongoose.Schema({
    name: String,
    children: [
        {
            name: String,
            value: [Number]
        }
    ]
})

const Map = mongoose.model('Map', mapSchema)

const rankSchema = new mongoose.Schema({
    name: String,
    value: Number
})

const Rank = mongoose.model('Rank', rankSchema)


const hotSchema = new mongoose.Schema({
    name: String,
    children: []
})

const Hot = mongoose.model('Hot', hotSchema)

const stockSchema = new mongoose.Schema({
    name: String,
    stock: Number,
    sales: Number
})

const Stock = mongoose.model('Stock', stockSchema)

exports.Budget = Budget
exports.Seller = Seller
exports.Trend = Trend
exports.Map = Map
exports.Rank = Rank
exports.Hot = Hot
exports.Stock = Stock