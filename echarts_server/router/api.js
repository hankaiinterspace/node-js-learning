const express = require('express')

const router = express.Router()
const apiHandler = require('../router_handler/api')

// 
router.get('/budget', apiHandler.budget)

// 商家销量
router.get('/seller', apiHandler.seller)

router.get('/trend', apiHandler.trend)


router.get('/map', apiHandler.map)
router.get('/chinamap', apiHandler.chinamap)
router.get('/provincemap/:name', apiHandler.provincemap)

router.get('/rank', apiHandler.rank)
router.post('/updateRank', apiHandler.updateRank)

router.get('/hot', apiHandler.hot)

router.get('/stock', apiHandler.stock)

module.exports = router