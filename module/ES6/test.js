// 引入其他的模块
// 如果是单个的js文件 引入时要加上后缀
// 引入的是一个npm下载的包，就不需要加后缀
import {foo, bar} from './module1.js'
import {fun, fun2} from './module2.js'
import module3 from './module3.js'
import $ from 'jquery'
import express from 'express'

foo();
bar();
fun();
fun2();
console.log(module3.msg);;
console.log(module3.fun());