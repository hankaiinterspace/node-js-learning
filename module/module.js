// console.log(module);

// 导入 moment包
const moment = require('moment')

// 调用moment()方法，得到当前时间
// 针对当前的时间，调用format() 方法，按照指定的格式进行时间的格式化
const dt = moment().format('YYYY-MM-DD HH:mm:ss')

console.log(dt);