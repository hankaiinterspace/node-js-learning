const express = require('express')
const app = express()
const session = require('express-session')
// 配置session中间件
app.use(session({
    secret: 'keyboard cat', // secret 属性的值可以为任意字符串
    resave: false, // 固定写法
    saveUninitialized: true // 固定写法
}))

// 托管静态页面
app.use(express.static('./pages'))
// 解析 POST 提交过来的表单数据
app.use(express.urlencoded({ extended: false }))

app.post('/api/login', (req, res) => {
    // 判断用户提交的登录信息是否正确
    if (req.body.username !== 'admin' || req.body.password !== '000000') {
        return res.send({ status: 1, msg: '登录失败' })
    }

    // 注意：只有成功配置了 express-session 这个中间件之后，才能够通过 req 点出来 session
    req.session.user = req.body // 将用户的信息，存储到 session 中
    req.session.isLogin = true  // 将用户的登陆状态，存储到 session 中

    res.send({ status: 0, msg: '登陆成功' })
})

// 获取用户姓名的接口
app.get('/api/username', (req, res) => {
    // 判断用户是否登录
    if (!req.session.isLogin) {
        return res.send({ status: 1, msg: 'fail' })
    }

    res.send({
        status: 0,
        msg: 'success',
        username: req.session.user.username
    })
})

// 退出登录的接口
app.post('/api/logout', (req, res) => {
    // 清空当前客户端对应的 session 信息
    req.session.destroy()
    res.send({
        status: 0,
        msg: '退出登录成功'
    })
})

app.listen(8080, () => {
    console.log('Express server running at http://127.0.0.1');
})