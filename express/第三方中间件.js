const express = require('express')

const app = express()

const parser = require('body-parser')

// parse application/x-www-form-urlencoded
app.use(parser.urlencoded({ extended: false }))

app.get('/list', function (req, res) {
    // res.send(req.query)
    res.send(req.body)
})

app.post('/add', function (req, res) {
    // res.send('got a post request')
    res.send(req.body)
})

// 启动web服务器
app.listen(8080, () => {
    console.log('server is running');
})