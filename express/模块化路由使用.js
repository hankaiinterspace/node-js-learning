const express = require('express')

const app = express()
const userRouter = require('./router')

// 使用 app.use() 注册路由模块
app.use(userRouter)

// 启动web服务器
app.listen(8080, () => {
    console.log('server is running');
})