const express = require('express')
// 创建路由对象
const router = express.Router()

// 挂载获取用户列表的路由
router.get('/user/list', function (req, res) {
    res.send('get user list')
})
router.post('/user/add', function (req, res) {
    res.send('Add new user')
})

// 向外导出路由对象
module.exports = router