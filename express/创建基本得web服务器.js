// 1. 导入 express
const express = require('express')

// 创建 web 服务器
const app = express()

// 参数1：客户端请求的url 地址
// 参数2：请求对应的处理函数
// req: 请求对象 (包含了与请求相关的属性与方法)
// res: 响应对象 (包含了响应相关的属性和方法)
// app.get('请求URL', function (req, res) { /* 处理函数 */})

app.get('/user', (req, res) => {
    res.send({ name: 'zs', age: 20, gender: '男'})
})

app.post('/user', (req, res) => {
    res.send('请求成功')
})


app.get('/', (req, res) => {
    // req.query 默认是一个空对象
    // 客户端使用 ?name=zs&age=20 这种查询字符串的形式，发送到服务器的参数。
    // 可以通过 req.query 对象访问到，例如：
    // req.query.name req.query.age
    console.log(req.query);
})

// URL 地址中，可以通过：参数名的形式，匹配动态参数值
app.get('/user/:id', (req, res) => {
    // req.params 默认是一个空对象
    // 里面存放着通过：动态匹配的参数值
    console.log(req.params);
})

// 调用 app.listen(端口号，启动成功得回调函数),启动服务器
app.listen(8080, () => {
    console.log('express server running at http://127.0.0.1');
})