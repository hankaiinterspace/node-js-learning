const express = require('express')

const app = express()

app.use(express.static('clock'))

app.listen(8080, () => {
    console.log('server is running');
})