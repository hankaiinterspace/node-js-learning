const express = require('express')

const app = express()

// 挂在路由
app.get('/', function (req, res) {
    res.send('hello world!')
})

app.post('/', function (req, res) {
    res.send('got a post request')
})

// 启动web服务器
app.listen(8080, () => {
    console.log('server is running');
})