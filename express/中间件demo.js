const express = require('express')
const userRouter = require('./router')
const app = express()
// 常量 mw 所指向的，就是一个中间件函数
const mw = function (req, res, next) {
    console.log('这是一个简单的中间件函数');
    // 注意：在当前中间件的业务处理完毕后，必须调用 next() 函数
    // 表示把流转关系转交给下一个中间件或路由
    next()
}

// 全局生效的中间件
app.use(mw)

// 使用 app.use() 注册路由模块
app.use(userRouter)

app.listen(8080, () => {
    console.log('server is running');
})
