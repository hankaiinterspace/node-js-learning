const express = require('express')
const app = express()
const bodyParser = require('./自定义中间件')

app.use(bodyParser)

app.get('/', (req, res) => {
    console.log('自定义中间件的测试');
    res.send(req.body)
})

app.listen(8080, () => {
    console.log('server is running');
})
