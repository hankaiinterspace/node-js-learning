const express = require('express')
const router = express.Router()



// bind your router here ...
router.get('/get', (req, res) => {
    // 获取客户端通过查询字符串，发送到服务器的数据
    const query = req.query
    // 调用 res.send() 方法，把数据响应给客户端
    res.send({
        status: 200, // 状态 200 表示成功过
        msg: 'GET请求成功',
        data: query
    })
})

router.post('/post', (req, res) => {
    // 获取客户端通过请求体，发送到服务器的URL-encoded数据
    const body = req.body
    // 调用 res.send() 方法，把数据响应给客户端
    res.send({
        status: 200, // 状态 200 表示成功过
        msg: 'GET请求成功',
        data: body
    })
})

module.exports = router