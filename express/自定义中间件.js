    // 自定义中间件.js 模块中的代码
    const qs = require('querystring')
    function bodyParser(req, res, next) {
        // 定义变量，用来存储客户端发送来的请求体数据
        let str = ''
        // 监听 req 对象的 data 事件(客户端发送过来的新的请求体数据)
        req.on('data', (chunk) => {
            // 拼接请求体数据，隐式转换为字符串
            str += chunk
        })
        // 监听 req 对象的 end 事件（请求体发送完毕自动触发）
        req.on('end', () => {
            // 打印完整的请求体数据
            console.log(str)
            const body = qs.parse(str) // 调用 qs.parse() 方法，把查询字符串解析为对象
            req.body = body  // 将解析出来的请求体对象，挂载为 req.body 属性
            next()  // 最后，一定要调用 next() 函数，执行后续的业务逻辑
        })
    }
    module.exports = bodyParser // 向外导出解析请求体数据的中间件函数