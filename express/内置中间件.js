const express = require('express')

const app = express()

// 配置json格式中间件
app.use(express.json())
// 配置 x-www-form-urlencoded 格式的内置中间件
app.use(express.urlencoded({ extended: false }))

// 挂在路由
app.get('/list', function (req, res) {
    // res.send(req.query)
    res.send(req.body)
})

app.post('/add', function (req, res) {
    // res.send('got a post request')
    res.send(req.body)
})

// 启动web服务器
app.listen(8080, () => {
    console.log('server is running');
})