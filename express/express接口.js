const express = require('express')
const app = express()
const apiRouter = require('./apiRouter')
// 导入cors中间件
const cors = require('cors')

// 先注册url-encoded中间件
app.use(express.urlencoded({ extended: false }))

// 注册cors中间件
app.use(cors())

app.use('/api', apiRouter)



app.listen(8080, () => {
    console.log('Express server running at http://127.0.0.1');
})