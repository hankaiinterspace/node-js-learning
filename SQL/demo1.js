const mysql = require('mysql')

// 建立与mysql数据库的连接
const db = mysql.createPool({
    host: '127.0.0.1',
    user: 'root',
    password: 'root',
    database: 'test' // 指定要操作哪个数据库
})
// 检测db.query()函数，指定要执行的sql语句，通过回调函数拿到执行的结果
// db.query('select 1', (err, res) => {
//     if (err) return console.log(err.message);
//     console.log(res);
// })

// db.query('select * from account', (err, res) => {
//     if (err) return console.log(err.message);
//     console.log(res);
// })  

// 要插入到account中的数据
// const user = { id: 2, username: 'iron-manDad', salary: '30000'}
// const sqlStr = 'update account set ? where id=?'
// db.query(sqlStr, [user, user.id], (err, res) => {
//     if (err) return console.log(err.message);
//     if (res.affectedRows === 1) console.log('更新数据成功');
// })


// const sqlStr = 'delete from account where id=?';
// db.query(sqlStr, 1, (err, res) => {
//     if (err) return console.log(err.message);
//     if (res.affectedRows === 1) console.log('删除数据成功');
// })


db.query('update account set status=1 where id=?', 2, (err, res) => {
    if (err) return console.log(err.message);
    if (res.affectedRows === 1) console.log('删除数据成功');
})
